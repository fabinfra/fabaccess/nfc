﻿using NFC;
using NFC.Helper;
using NFC.Helper.Crypto;
using NFC.Interfaces;
using NUnit.Framework;
using System;
using System.Text;
using NFC.PCSC;
using NFC.Cards.NXP_MIFARE_DESFire.Enums;
using NFC.Cards.NXP_MIFARE_DESFire;

namespace NFC_Test.REAL
{
    /// <summary>
    /// Test all DESFire Commands with an Empty nfcService
    /// The Test are ordered to check the Commands one by one
    /// </summary>
    [TestFixture, Explicit]
    public class REAL_EncFile
    {
        /// <summary>
        /// Set ReaderID for PCSC Interface
        /// You can get the ID from REAL_Reader_PCSC
        /// </summary>
        public readonly string ReaderID = "ACS ACR122 0";// "ACS ACR122U PICC Interface 0";

        #region Fixed Config Properties
        public readonly UInt32 ApplicationID = 0xAAFFEE;
        public readonly string ApplicationMasterKey = "25432A462D4A614E645267556B587032";
        public readonly string ApplicationKey_1 = "25432A462D4A614E645267556B587032";
        public readonly byte FileID = 0x01;
        public readonly byte FileSize = 0xF0;
        #endregion

        [Test, Order(1)]
        public void CreateFile()
        {
            INFCService nfcService = new NFCService();

            nfcService.Connect(ReaderID);

            NXP_MIFARE_DESFire desfire = new NXP_MIFARE_DESFire(nfcService);

            desfire.SelectApplication(0x000000);

            CipherKey key = new CipherKey(CipherType.TDES);
            desfire.AuthenticateISO_DES(0x00, key._Key);

            desfire.Format();

            desfire.AuthenticateISO_DES(0x00, key._Key);

            byte keysetting1 = desfire.GenerateKeySetting1(ChangeApplicationKey.MASTERKEY, ChangeMasterKeySettings.WITHMASTERKEY, CreateDeleteFile.ONLYMASTERKEY, FileDirectoryAccess.NOKEY, ChangeMasterKey.CHANGEABLE);
            byte keysetting2 = desfire.GenerateKeySetting2(CryptoOperationsType.AES, FileIdentifies.NOTUSED, 2);

            desfire.CreateApplication(ApplicationID, keysetting1, keysetting2);

            desfire.SelectApplication(ApplicationID);

            CipherKey key_aes = new CipherKey(CipherType.AES);
            desfire.AuthenticateISO_AES(0x00, key_aes._Key);

            UInt16 accesRights = desfire.GenerateFileAccessRights((byte)FileAccessRights.FREE, 0x00, 0x00, 0x00);
            desfire.CreateFile_Standard(FileID, FileCommunication.ENCRYPT, accesRights, FileSize);

            desfire.WriteData(FileID, 0, Encoding.ASCII.GetBytes("Test1234"));

            APDUCommand cmd_WriteData = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0x3D,
                Data = ByteOperation.GenerateEmptyArray(8)
            };
            Console.WriteLine(cmd_WriteData.ToString());
            APDUResponse response = nfcService.Transmit(cmd_WriteData);
            Console.WriteLine(response.ToString());

            byte[] data = desfire.ReadData(FileID, 0, FileSize);
            Console.WriteLine(Encoding.ASCII.GetString(data).Replace("\u0000", ""));

            nfcService.Disconnect();
        }
    }
}
