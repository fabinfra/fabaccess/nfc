﻿using NFC.Interfaces;
using NFC.PCSC;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace NFC_Test.REAL
{
    [TestFixture, Explicit]
    public class REAL_Reader_PCSC
    {
        /// <summary>
        /// Print PCSC ReaderIDs to Console Out
        /// </summary>
        [Test]
        public void GetReaders()
        {
            INFCService nfcService = new NFCService();
            IList<string> readers = nfcService.GetReaderIDs();

            Console.WriteLine("PCSC Readers detected: {0}", readers.Count);

            if (readers.Count > 0)
            {
                Console.WriteLine("List of ReaderIDs:");
                foreach (string readerID in readers)
                {
                    Console.WriteLine("{0}", readerID);
                }
            }
        }

        /// <summary>
        /// Connect to specific PCSC Reader by ReaderID
        /// </summary>
        /// <param name="readerID">ReaderID from GetReaders</param>
        //[TestCase("ACS ACR122U PICC Interface 0")]
        [TestCase("ACS ACR122 0")]
        public void Connect(string readerID)
        {
            INFCService nfcService = new NFCService();

            nfcService.Connect(readerID);

            nfcService.Disconnect();
        }
    }
}
