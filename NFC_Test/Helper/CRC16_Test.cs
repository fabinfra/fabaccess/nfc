﻿using NFC.Helper;
using NFC.Helper.Crypto.CRC;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    [TestFixture]
    public class CRC16_Test
    {
        [Test]
        [Ignore("Unknown Expected Data")]
        public void Caluclate()
        {
            byte[] data = HexConverter.ConvertFromHexString("");
            byte[] crc_expected = HexConverter.ConvertFromHexString("");

            CRC16 crc16 = new CRC16();
            byte[] crc = crc16.Calculate(data);

            Assert.AreEqual(crc_expected, crc);
        }
    }
}
