﻿using NFC.Helper;
using NFC.Helper.Crypto.Cipher;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    [TestFixture]
    public class TDES_Test
    {
        [Test]
        [Ignore("Unknown Expected Data")]
        public void Encrypt_TDES()
        {
            byte[] data = HexConverter.ConvertFromHexString("");
            byte[] key = HexConverter.ConvertFromHexString("");
            byte[] iv = HexConverter.ConvertFromHexString("0000000000000000");

            TDES des = new TDES();
            byte[] data_enc = des.Encrypt(data, key, iv);

            byte[] data_enc_expected = HexConverter.ConvertFromHexString("");
            Assert.AreEqual(data_enc_expected, data_enc);
        }

        [Test]
        [Ignore("Unknown Expected Data")]
        public void Encrypt_TDES_2K()
        {
            byte[] data = HexConverter.ConvertFromHexString("");
            byte[] key = HexConverter.ConvertFromHexString("");
            byte[] iv = HexConverter.ConvertFromHexString("0000000000000000");

            TDES_2K des = new TDES_2K();
            byte[] data_enc = des.Encrypt(data, key, iv);

            byte[] data_enc_expected = HexConverter.ConvertFromHexString("");
            Assert.AreEqual(data_enc_expected, data_enc);
        }

        [Test]
        [Ignore("Unknown Expected Data")]
        public void Encrypt_TDES_3K()
        {
            byte[] data = HexConverter.ConvertFromHexString("");
            byte[] key = HexConverter.ConvertFromHexString("");
            byte[] iv = HexConverter.ConvertFromHexString("0000000000000000");

            TDES_3K des = new TDES_3K();
            byte[] data_enc = des.Encrypt(data, key, iv);

            byte[] data_enc_expected = HexConverter.ConvertFromHexString("");
            Assert.AreEqual(data_enc_expected, data_enc);
        }
    }
}