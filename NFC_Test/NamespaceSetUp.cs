﻿using log4net.Config;
using NUnit.Framework;

namespace NFC_Test
{
    /// <summary>
    /// Add log4net Output to Console Out
    /// </summary>
    [SetUpFixture]
    public class NamespaceSetUp
    {       
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            BasicConfigurator.Configure();
        }
    }
}

namespace NFC_Real_Test
{
    /// <summary>
    /// Add log4net Output to Console Out
    /// </summary>
    [SetUpFixture]
    public class NamespaceSetUp
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            BasicConfigurator.Configure();
        }
    }
}
