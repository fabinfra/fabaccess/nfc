﻿using System;
using NFC;
using NUnit.Framework;

namespace NFC_Test.Helper
{
    [TestFixture]
    public class APDUCommand_Test
    {
        [Test]
        public void Compare()
        {
            APDUCommand command1 = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0xAA,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            APDUCommand command2 = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0xAA,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            Assert.IsTrue(command1 == command2);
        }

        [Test]
        public void Compare_Diff()
        {
            APDUCommand command1 = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0xAA,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            APDUCommand command2 = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0x1A,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            Assert.IsFalse(command1 == command2);
        }

        [Test]
        public void ToString_Case1()
        {
            APDUCommand command = new APDUCommand(IsoCase.Case1)
            {
                CLA = 0x90,
                INS = 0x1A
            };

            Console.WriteLine(command.ToString());
        }

        [Test]
        public void ToString_Case2()
        {
            APDUCommand command = new APDUCommand(IsoCase.Case2Short)
            {
                CLA = 0x90,
                INS = 0x1A
            };

            Console.WriteLine(command.ToString());
        }

        [Test]
        public void ToString_Case3()
        {
            APDUCommand command = new APDUCommand(IsoCase.Case3Short)
            {
                CLA = 0x90,
                INS = 0x1A,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            Console.WriteLine(command.ToString());
        }

        [Test]
        public void ToString_Case4()
        {
            APDUCommand command = new APDUCommand(IsoCase.Case4Short)
            {
                CLA = 0x90,
                INS = 0x1A,
                Data = new byte[]
                {
                    0x01, 0x02, 0x03
                }
            };

            Console.WriteLine(command.ToString());
        }
    }
}
