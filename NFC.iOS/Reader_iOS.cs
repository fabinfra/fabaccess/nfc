﻿//using NFC.Interfaces;
//using System;
//using CoreNFC;
//using Foundation;

//namespace NFC_iOS
//{
//    public class Reader_iOS : NFCTagReaderSessionDelegate, IReader
//    {
//        public event ReaderEventHandler CardDiscovered;
//        public event ReaderEventHandler CardLost;

//        private NFCReaderSession _session = null;
//        private DispatchQueue _queue;

//        public void Start()
//        {
//            _queue = new DispatchQueue("NFC Reader Queue", true);

//            // sessions cannot be reused
//            _session = new NFCTagReaderSession(NFCPollingOption.Iso14443, this, _queue)
//            {
//                AlertMessage = "TODO",
//            };

//            if (_session == null)
//            {
//                Console.WriteLine("Oh no! The session is null!");
//            }

//            _session.BeginSession();
//        }

//        public void Stop()
//        {
//            _session?.InvalidateSession();
//            _session = null;
//        }

//        public override void DidDetectTags(NFCTagReaderSession session, INFCTag[] tags)
//        {
//            Console.WriteLine("Did detect tags");

//            Console.WriteLine(tags[0].Type);

//            //INFCIso7816Tag tag = tags[0].GetNFCIso7816Tag();
//            INFCMiFareTag tag = tags[0].GetNFCMiFareTag();
//            if (tag != null)
//            {
//                Console.WriteLine("Card ist valid");
//                CardDiscovered?.Invoke(this, new Card(session, tag));
//            }
//            else
//            {
//                Console.WriteLine("Card is not ISO7816");
//            }
//        }

//        public override void DidInvalidate(NFCTagReaderSession session, NSError error)
//        {
//            // TODO: decide what to do
//            Console.WriteLine("reader session invalidated");
//        }
//    }
//}
