﻿using System;

namespace NFC.Exceptions
{
    public class TransmitException : Exception
    {
        public TransmitException()
        {

        }

        public TransmitException(string message) : base(message)
        {

        }

        public TransmitException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
