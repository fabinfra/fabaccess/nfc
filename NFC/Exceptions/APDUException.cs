﻿using System;

namespace NFC.Exceptions
{
    public class APDUException : Exception
    {
        public readonly byte ResponseCode;
    }
}
