﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Current authentication status does not allow there- quested command.
    /// 0x91AE
    /// </summary>
    public class AuthenticationMissingException : Exception
    {
        public AuthenticationMissingException()
        {

        }

        public AuthenticationMissingException(string message) : base(message)
        {

        }

        public AuthenticationMissingException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
