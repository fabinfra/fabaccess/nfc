﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Length of command string invalid.
    /// 0x917E
    /// </summary>
    public class LengthErrorException : Exception
    {
        public LengthErrorException()
        {

        }

        public LengthErrorException(string message) : base(message)
        {

        }

        public LengthErrorException(string message, Exception inner) : base(message, inner)
        {
            
        }
    }
}
