﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Creation of file/application failed because file/application with same number already exists
    /// 0x91DE
    /// </summary>
    public class DuplicateErrorException : Exception
    {
        public DuplicateErrorException()
        {

        }

        public DuplicateErrorException(string message) : base(message)
        {

        }

        public DuplicateErrorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
