﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Attempt toread/write data from/to beyond thefile’s/record’s limits. Attempt to exceed the limitsof a value file.
    /// 0x91BE
    /// </summary>
    public class BoundaryErrorException : Exception
    {
        public BoundaryErrorException()
        {

        }

        public BoundaryErrorException(string message) : base(message)
        {

        }

        public BoundaryErrorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
