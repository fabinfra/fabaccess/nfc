﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// CRC or MAC does not match data. Paddingbytes not valid.
    /// 0x911E
    /// </summary>
    public class IntegrityErrorException : Exception
    {
        public IntegrityErrorException()
        {

        }

        public IntegrityErrorException(string message) : base(message)
        {

        }

        public IntegrityErrorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
