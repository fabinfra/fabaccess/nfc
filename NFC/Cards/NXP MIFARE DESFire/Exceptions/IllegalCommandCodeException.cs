﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Command code not supported.
    /// 0x911C
    /// </summary>
    public class IllegalCommandCodeException : Exception
    {
        public IllegalCommandCodeException()
        {

        }

        public IllegalCommandCodeException(string message) : base(message)
        {

        }

        public IllegalCommandCodeException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
