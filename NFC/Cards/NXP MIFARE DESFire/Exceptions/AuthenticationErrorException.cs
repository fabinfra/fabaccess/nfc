﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Current authentication status does not allow there- quested command.
    /// 0x91AE
    /// </summary>
    public class AuthenticationErrorException : Exception
    {
        public AuthenticationErrorException()
        {

        }

        public AuthenticationErrorException(string message) : base(message)
        {

        }

        public AuthenticationErrorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
