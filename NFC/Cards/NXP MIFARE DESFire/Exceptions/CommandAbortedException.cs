﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Previous Command was not fully completed.Not all Frames were requested or provided bythe PCD.
    /// 0x91CA
    /// </summary>
    public class CommandAbortedException : Exception
    {
        public CommandAbortedException()
        {

        }

        public CommandAbortedException(string message) : base(message)
        {

        }

        public CommandAbortedException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
