﻿using System;

namespace NFC.Cards.NXP_MIFARE_DESFire.Exceptions
{
    /// <summary>
    /// Value of the parameter(s) invalid.
    /// 0x919E
    /// </summary>
    public class ParameterErrorException : Exception
    {
        public ParameterErrorException()
        {

        }

        public ParameterErrorException(string message) : base(message)
        {

        }

        public ParameterErrorException(string message, Exception inner) : base(message, inner)
        {

        }
    }
}
