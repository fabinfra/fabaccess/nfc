﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// codes whether the application master key is changeable
    /// </summary>
    public enum ChangeMasterKey : byte
    {
        /// <summary>
        /// Application master key is not changeable anymore (frozen)
        /// </summary>
        FROZEN = 0x00,

        /// <summary>
        /// Application master key is changeable (authentication with the current application master key necessary, default)
        /// </summary>
        CHANGEABLE = 0x01,
    }
}
