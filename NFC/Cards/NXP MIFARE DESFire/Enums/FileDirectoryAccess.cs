﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// codes whether application master key authentication is needed for file directory access
    /// </summary>
    public enum FileDirectoryAccess : byte
    {
        /// <summary>
        /// Successful application master key authentication is required for executing the “Get FID List”, “Get File Settings”and “Get Key Settings”commands
        /// </summary>
        ONLYMASTERKEY = 0x00,

        /// <summary>
        /// “Get FID List”, “Get File Settings” and “Get Key Settings” commands succeed independentlyof a preceding application master key authentication (default)
        /// </summary>
        NOKEY = 0x02,
    }
}
