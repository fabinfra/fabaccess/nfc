﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// codes whether application master key authentication is needed before “Create File” / “Delete File”
    /// </summary>
    public enum CreateDeleteFile : byte
    {
        /// <summary>
        /// “Create File”/ “Delete File”is permitted only with application master key authentication
        /// </summary>
        ONLYMASTERKEY = 0x00,

        /// <summary>
        /// “Create File”/ “Delete File”is permitted also without application master key authentication (default)
        /// </summary>
        NOKEY = 0x04,
    }
}
