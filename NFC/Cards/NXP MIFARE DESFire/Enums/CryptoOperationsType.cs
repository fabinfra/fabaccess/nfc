﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// Crypto method of the application
    /// </summary>
    public enum CryptoOperationsType : byte
    {
        TDES = 0x00,
        TKTDES = 0x40,
        AES = 0x80,
    }
}