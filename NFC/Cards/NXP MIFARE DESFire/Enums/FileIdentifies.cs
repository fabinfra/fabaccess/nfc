﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    /// <summary>
    /// Indicates use of 2 byte ISO/IEC 7816-4 File Identifies for files within the Application
    /// </summary>
    public enum FileIdentifies : byte
    {
        NOTUSED = 0x00,
        USED = 0x20
    }
}
