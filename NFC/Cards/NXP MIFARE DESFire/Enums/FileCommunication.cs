﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    public enum FileCommunication : byte
    {
        /// <summary>
        /// "Plain communication"
        /// </summary>
        PLAIN = 0x00,

        /// <summary>
        /// Plain communication secured by DES/3DES MACing
        /// </summary>
        MAC = 0x01,

        /// <summary>
        /// Fully DES/3DES enciphered communication
        /// </summary>
        ENCRYPT = 0x03
    }
}
