﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    public enum FileAccessRights : byte
    {
        FREE = 0x0E,
        NEVER = 0x0F
    }
}
