﻿namespace NFC.Cards.NXP_MIFARE_DESFire.Enums
{
    enum FileTypes : byte
    {
        /// <summary>
        /// Standard Data File
        /// </summary>
        STANDARD =  0x00,

        /// <summary>
        /// Backup Data Files
        /// </summary>
        BACKUP = 0x01,

        /// <summary>
        /// Value Files with Backup
        /// </summary>
        VALUE = 0x02,

        /// <summary>
        /// Linear Record Files with Backup
        /// </summary>
        LINEARRECORD = 0x03,

        /// <summary>
        /// Cyclic Record Files with Backup
        /// </summary>
        CYCLICRECORD = 0x04
    }
}
