﻿namespace NFC
{
    /// <summary>
    /// https://github.com/danm-de/pcsc-sharp/blob/246fc4303190184d6acd98a2d66f48cb7ffd7094/src/PCSC/SCardProtocol.cs
    /// </summary>
    public enum SCardProtocol
    {
        UNSET,
        T0,
        T1,
        RAW,
        T15,
        ANY
    }
}
