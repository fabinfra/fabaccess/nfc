﻿namespace NFC
{
    /// <summary>
    /// https://github.com/danm-de/pcsc-sharp/blob/246fc4303190184d6acd98a2d66f48cb7ffd7094/src/PCSC.Iso7816/IsoCase.cs
    /// </summary>
    public enum IsoCase
    {
        Case1,
        Case2Short,
        Case3Short,
        Case4Short,
        Case2Extended,
        Case3Extended,
        Case4Extended
    }
}
