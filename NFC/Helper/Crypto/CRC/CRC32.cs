﻿using System;

namespace NFC.Helper.Crypto.CRC
{
    /// <summary>
    /// CRC32 for DESFire Card
    /// </summary>
    public class CRC32
    {
        public UInt32 Polynomial { get; } = 0xEDB88320;

        public UInt32 InitValue { get; } = 0xFFFFFFFF;

        public UInt32 Calculate(byte[] data, UInt32 crc32)
        {
            for (int i = 0; i < data.Length; i++)
            {
                crc32 ^= data[i];
                for (int b = 0; b < 8; b++)
                {
                    bool b_Bit = (crc32 & 0x01) > 0;
                    crc32 >>= 1;
                    if (b_Bit)
                    {
                        crc32 ^= Polynomial;
                    }
                }
            }
            return crc32;
        }

        public byte[] Calculate(params byte[][] data)
        {
            UInt32 crc32 = InitValue;

            foreach(byte[] d in data)
            {
                crc32 = Calculate(d, crc32);
            }

            return BitConverter.GetBytes(crc32);
        }
    }
}
