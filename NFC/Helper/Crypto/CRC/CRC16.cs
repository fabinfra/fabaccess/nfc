﻿using System;

namespace NFC.Helper.Crypto.CRC
{
    /// <summary>
    /// CRC16 for DESFire Card
    /// </summary>
    public class CRC16
    {
        public UInt16 Polynomial { get; } = 0x8408;

        public UInt16 InitValue { get; } = 0x6363;

        public UInt16 Calculate(byte[] data, UInt16 crc16)
        {
            for (int i = 0; i < data.Length; i++)
            {
                crc16 ^= data[i];
                for (int b = 0; b < 8; b++)
                {
                    bool b_Bit = (crc16 & 0x01) > 0;
                    crc16 >>= 1;
                    if (b_Bit)
                    {
                        crc16 ^= Polynomial;
                    }
                }
            }
            return crc16;
        }

        public byte[] Calculate(params byte[][] data)
        {
            UInt16 crc16 = InitValue;

            foreach(byte[] d in data)
            {
                crc16 = Calculate(d, crc16);
            }

            return BitConverter.GetBytes(crc16);
        }
    }
}
