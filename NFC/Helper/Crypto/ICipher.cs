﻿namespace NFC.Helper.Crypto
{
    public interface ICipher
    {
        /// <summary>
        /// Size of Cipher Block in Byte
        /// </summary>
        uint BlockSize { get; }

        /// <summary>
        /// Size of Key in Byte
        /// </summary>
        uint KeySize { get; }

        /// <summary>
        /// Encrypt Data
        /// </summary>
        /// <param name="data">Data in BlockSize</param>
        /// <param name="key">Key</param>
        /// <param name="IV">Initialisation Vector</param>
        /// <returns></returns>
        byte[] Encrypt(byte[] data, byte[] key, byte[] IV);

        /// <summary>
        /// Decrypt Data
        /// </summary>
        /// <param name="data">Data in BlockSize</param>
        /// <param name="key">Key</param>
        /// <param name="IV">Initialisation Vector</param>
        byte[] Decrypt(byte[] data, byte[] key, byte[] IV);
    }
}
