﻿using System.Collections.Generic;
using System;
using NFC.Exceptions;

namespace NFC.Interfaces
{
    /// <summary>
    /// Service to provide hardware specific NFC Interfaces
    /// </summary>
    public interface INFCService
    {
        /// <summary>
        /// Indicates if NFC Hardware is available
        /// </summary>
        bool IsAvailable { get; }

        /// <summary>
        /// Indicates if NFC Hardware is enabled
        /// </summary>
        bool IsEnabled { get; }

        /// <summary>
        /// Indicates if NFC Card is connected
        /// </summary>
        bool IsConnected { get; }

        /// <summary>
        /// Get a list of availible NFC ReaderIDs
        /// </summary>
        IList<string> GetReaderIDs();

        /// <summary>
        /// Connect with ReaderID to NFC Card
        /// </summary>
        /// <param name="readerID">ReaderID from GetReaderIDs</param>
        /// <exception cref="ReaderException"></exception>
        /// <exception cref="CardException"></exception>
        /// <exception cref="ConnectionException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        void Connect(string readerID);

        /// <summary>
        /// Disconnects Reader from NFC Card
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="ConnectionException"></exception>
        void Disconnect();

        /// <summary>
        /// Transmit APDUCommand to Card if connected
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// <exception cref="TransmitException"></exception>
        /// <exception cref="CardException"></exception>
        /// <exception cref="ReaderException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        APDUResponse Transmit(APDUCommand command);
    }
}
