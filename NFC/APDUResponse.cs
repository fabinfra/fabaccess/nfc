﻿using System;

namespace NFC
{
    public class APDUResponse
    {
        public byte SW1 { get; set; }
        public byte SW2 { get; set; }
        public byte[] Body { get; set; }

        public override string ToString()
        {
            if(Body != null)
            {
                return string.Format("SW: {0:x} {1:x} Body: {2:x}", SW1, SW2, BitConverter.ToString(Body).Replace("-", "").ToLower());
            }
            else
            {
                return string.Format("SW: {0:x} {1:x} Body: null", SW1, SW2);
            }
            
        }
    }
}
